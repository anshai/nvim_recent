if exists('g:loaded_nvim_recent')
  finish
endif

:fun! s:nvim_recent()
: lua require'nvim_recent'.run()
:endfun

:command NvimRecent :call s:nvim_recent()

let g:loaded_nvim_recent = 1
let g:nvim_recent_verison = 0.0.1
